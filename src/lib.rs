#![warn(missing_docs)]
#![warn(missing_doc_code_examples)]

//! A crate for dealing with the Gemini (and Mercury) protocol
//!
//! This crate is split into four parts:
//! - [`gemtext`]
//! - [`url`]
//! - [`protocol`]
//! - [`request`]
//!
//! The [`gemtext`] module is used purely for the parsing of gemtext.
//! The [`protocol`] module is used for accessing and parsing various structures to implement the
//! gemini protocol. This module does not use networking
//! The [`request`] module is used for actually making requests using [`std::net`]. This can be
//! disabled by disabling the feature flag `net`.
//! The [`url`] module is used for manipulating URLs and only contains the areas of the URL specification
//! that Gemini requires.
//!
//! # Example
//! ```no_run
//! use gmi::*;
//! use std::convert::TryFrom;
//! fn main() -> Result<(), request::RequestError> {
//!     let test_url = "gemini://gemini.circumlunar.space";
//!     let mut test_url = url::Url::try_from(test_url).unwrap();
//!     println!("Making response with URL: {}", test_url);
//!     loop {
//!         let response = request::make_request(&test_url)?;
//!         match response.status {
//!             protocol::StatusCode::Redirect(c) => {
//!                 println!("Redirect! Code: {} with meta {}", c, response.meta);
//!                 test_url = url::Url::try_from(response.meta.as_str()).unwrap();
//!                 println!("New URL: {}", test_url);
//!             }
//!             protocol::StatusCode::Success(c) => {
//!                 println!("Success! Code: {} with MIME type: {}", c, response.meta);
//!                 println!("{}", String::from_utf8_lossy(&response.data));
//!                 break;
//!             },
//!             s => panic!("Unknown status code: {:?}", s),
//!         }
//!     }
//! 
//!     return Ok(())
//! }
//! ```
pub mod gemtext;
pub mod url;
pub mod protocol;
#[cfg(feature = "net")]
pub mod request;

/*
#[cfg(test)]
mod tests {
    #[test]
    fn test() {
        use std::io::prelude::*;
        let mut stream = std::net::TcpStream::connect("oppen.digital:1963").unwrap();
        stream.write(b"mercury://oppen.digital/index.gmi").unwrap();
        let mut stream_data = [0;1024];
        stream.read(&mut stream_data).unwrap();
        let resp = crate::req_resp::Response::parse_string(&stream_data).unwrap();

        panic!("{}", String::from_utf8_lossy(&resp.data).into_owned());
    }
}
*/
