# Contributing to `gmi`

So you wanna contribute to `gmi`? Great! Here's some steps that you can follow for doing it

## Contributions other than code:

There are so many other ways than code that you can do to contribute to open source.

### Bug Hunting
Bugs are a part of any library and this is no exception. If you ever find any bugs in the library, please report them! I'd love to hear about them and fix them. Open up an issue and fill out the existing template.

### Feature Requests
If you have a request for a feature, open up an issue for it! We'll start a discussion and we can go from there. Keep in mind that lightweight is a key factor in this library, and if your feature is not allowed for reasons of feature creep, do not fret! Try and create your own library extending from this one! And let us know, because that'd be pretty cool.

### Branding
Some branding for this project would be really cool, and greatly appreciated! If you have some ideas, open up an issue and let us know!

## Contributing Code:

And now to get started with contributing code. Much of this is inspired directly by [this article by Drew DeVault](gemini://drewdevault.com/2021/01/02/2021-01-02-Every-commit-should-be-perfect.gmi) (to open this you will need a gemini client. [Here is the same article through a proxy](https://portal.mozz.us/gemini/drewdevault.com/2021/01/02/2021-01-02-Every-commit-should-be-perfect.gmi)).

I also highly recommend checking out [this webpage about using `git rebase` effectively](https://git-rebase.io/). This is not necessary reading, but is just good information

### Pull Requests
Pull requests must contain a singular commit. This means before doing a pull request, you should squash all your commits. You can see how to do that in the `git rebase` article I mentioned above. I recommend when you have to make some sort of change to your commit, you should use `git --ammend`.

### Commit messages
Commit messages should be done as follows:
```
subsystem: Short and sweet message

I replaced foo with baz because the discussion in issue #5.
I am just filling up text because I want to show that you need
to provide a well done description of what you did in your
commit messages
```
This change was done in subsystem `subsystem` and contains a super short message about the exact change that happened. This will make it really easy to do release notes by using `git shortlog`

### Bug fix specifics
Any bug fixes that are done should have a test created to test any regressions for it. Of course this isn't possible for all bugs, but if possible, it should

### General coding things:

#### Warnings are to be avoided!
The rust compiler has warnings for a reason! Unless there's a good reason to ignore a warning, I recommend fixing it prior to putting in your pull request.

#### Documentation!
If you create a new function, struct, etc., it should be fully documented with examples if possible. A warning will come up for any undocumented entities!

#### Avoiding things such as Mutex and Arc
Gemini is simple enough to where you should be able to effectively avoid these constructs. If there's a good reason to keep one, though, I won't stop you.

