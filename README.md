# gmi
`gmi` (pronounced as "Jee Mee" or however you'd like) is a simple to use, lightweight Rust library for interfacing with the [Gemini protocol](https://gemini.circumlunar.space/) as a client. It was created out of frustration with exsisting libraries bringing in multiple other dependencies that I personally saw as unnecessary. It is NOT meant to be used as a library to run as a server.

This library is split into three main parts:
- `gemtext`: A [Gemtext](https://gemini.circumlunar.space/docs/gemtext.gmi) parser
- `protocol`: A collection of the various data structures that are used by the gemini protocol.
- `request`: A module for actually making requests to gemini servers. This can be disabled by disabling the feature flag `net`.
- `url`: A simple implementation of a subset of the [URL RFC](https://datatracker.ietf.org/doc/html/rfc3986) implementing only what is needed by Gemini.


## Focuses
- Lightweight
- Small external dependencies
- Simple interface

## Features:
- [x] Gemini TLS support
- [x] Gemini Response parsing
- [x] Gemtext parsing
- [ ] Client certificates
- [ ] Trust On First Use (TOFU)
- [x] [Mercury](https://portal.mozz.us/gemini/gemini.circumlunar.space/users/solderpunk/gemlog/the-mercury-protocol.gmi) support

## Simple Example:
```rust
use gmi::url::Url;
use gmi::request;
fn main() {
    let url = Url::from_str("gemini://gemini.circumlunar.space").unwrap();
    let resp = request::make_request(&url).unwrap();
    println!("{}", string::from_utf8_lossy(&resp.data));
}
```

## Alternatives
[`gemini`](https://crates.io/crates/gemini/) is a bit of a heavier, but more feature complete library for interfacing with the Gemini protocol. It can be used for both a server and a client, unlike `gmi` which can only be used as a client (for now (; )
